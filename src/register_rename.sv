module register_rename(

    input logic clk_i,
    input logic rst_ni,

    input logic flush_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] valid,

    // Register Rename Pins

    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(rvp1_pkg::NR_ARF)-1:0] read_reg_i,
    output logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] read_rename_reg_o,

    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_ARF)-1:0] write_reg_i,
    output logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] write_rename_reg_o,

    // Tell Decode Stage that ROB is full
    output logic rob_full_o,

    // Issue Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_issued_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] issue_valid_i,

    // Exec Stage Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_finished_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] finish_valid_i,

    // Commit Stage Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_commit_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] commit_valid_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] orig_reg_i,

    // Pins to communicate with ROB whether Data is ready from execution stage
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_ready_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0] ready_valid_i,
    output logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0] reg_ready_o,
    
    // Tell Decode Stage all instructions have not been serviced
    output logic stall_for_instr_o

    // Retirement Pins
);
    
logic [rvp1_pkg::NR_RRF-1:0] rob_mask_d, rob_mask_q;

rvp1_pkg::rob_entry_t [rvp1_pkg::NR_RRF-1:0] rob_bits_d, rob_bits_q;

logic [rvp1_pkg::INSTR_PER_FETCH-1:0] serviced_d, serviced_q;

logic [rvp1_pkg::NR_ARF-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] rat_d, rat_q;
logic [rvp1_pkg::NR_ARF-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] retirement_rat_d, retirement_rat_q;

assign rob_full_o = &rob_mask_q;

// Fetch the Renamed Read Reg from File
always_comb begin
    // Check for RAW dependencies
    automatic logic [1:0] depends = '0;
    read_rename_reg_o = '0;

    for (int unsigned i = 1; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        if (valid[i]) begin
            for (int j = 0; j < i; j++) begin
                for (int k = 0; k < 2; k++) begin
                    if (read_reg_i[i][k] == write_reg_i[j]) begin
                        read_rename_reg_o[i][k] = write_rename_reg_o[j];
                        depends[k] = 1'b1;
                    end
                end
            end

            for (int unsigned k = 0; k < 2; k++) begin
                if (!depends[k]) begin
                    read_rename_reg_o[i][k] = rat_q[read_reg_i[i][k]];
                end
            end
        end
    end
end

assign stall_for_instr_o = &serviced_q;

// Fetch New Name for Write Register if it is being written into TODO
// We need to find one for all four instructions
always_comb begin

    rat_d = rat_q;
    retirement_rat_d = retirement_rat_q;
    rob_bits_d = rob_bits_q;
    rob_mask_d = rob_mask_q;
    serviced_d = serviced_q;

    for (int unsigned i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        if (valid[i]) begin
            for (int j = (rvp1_pkg::NR_RRF - 1); j >= 0 ; j--) begin
                if (write_reg_i != '0 && !rob_full_o && !serviced_q[i]) begin
                    if (!rob_mask_q[j]) begin
                        automatic logic [$clog2(rvp1_pkg::NR_RRF)-1:0] old_index = rat_q[write_reg_i[i]];
                        
                        // TODO: Do we need to do this?
                        // Because we do have the orig bit
                        rob_bits_d[old_index].old = 1'b1;
      
                        write_rename_reg_o[i] = j[$clog2(rvp1_pkg::NR_RRF)-1:0];
                        rat_d[write_reg_i[i]] = j[$clog2(rvp1_pkg::NR_RRF)-1:0];
                        rob_mask_d[j] = 1;
                        rob_bits_d[j].issued = 1'b0;
                        rob_bits_d[j].finished = 1'b0;
                        rob_bits_d[j].old = 1'b0;
                        serviced_d[i] = 1'b1;
                    end
                end
            end
        end
    end

    // Cleanup any old regs that were finished up
    for (int unsigned i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        automatic rvp1_pkg::rob_entry_t i_bits;
        i_bits = rob_bits_q[i];

        if (!(i_bits.orig) && i_bits.finished) begin
            rob_mask_d[i] = 0;
        end
    end

    for (int unsigned i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        // Issue Bits
        if (issue_valid_i[i])
            rob_bits_d[reg_issued_i].issued = 1'b1;

        // Finish Bits
        if (finish_valid_i[i])
            rob_bits_d[reg_finished_i].finished = 1'b1;

        // Commit Routine
        if (commit_valid_i[i]) begin
            // Write Changes to Retirement RAT
            automatic logic [$clog2(rvp1_pkg::NR_RRF)-1:0] rob_id = reg_commit_i[i];
            retirement_rat_d[orig_reg_i[i]] = rob_id;
        
            // Update Bits
            rob_bits_d[rob_id].orig = 1'b1;
            rob_bits_d[rob_id].finished = 1'b1;
            rob_bits_d[rob_id].issued = 1'b1;
            rob_mask_d[rob_id] = 1'b1;
        end
    end
end

always_comb begin: ready_logic
    
    for (int unsigned i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        for (int unsigned j = 0; j < 2; j++) begin
            if (ready_valid_i[i][j] && rob_bits_q[reg_ready_i[i][j]].finished) begin
                reg_ready_o[i][j] = 1'b1;
            end
        end
    end

end

always_ff @ (posedge clk_i or negedge rst_ni) begin
    if (~rst_ni) begin
        for (int unsigned i = 0; i < rvp1_pkg::NR_ARF; i++) begin
            rat_q[i] <= i[$clog2(rvp1_pkg::NR_RRF)-1:0];
            retirement_rat_q[i] <= i[$clog2(rvp1_pkg::NR_RRF)-1:0];
        end
        rob_mask_q <= '0;
        rob_bits_q <= '0;
        serviced_q <= '0;
    end else begin
        if (flush_i) begin
            for (int unsigned i = 0; i < rvp1_pkg::NR_ARF; i++) begin
                rat_q[i] <= i[$clog2(rvp1_pkg::NR_RRF)-1:0];
            end
        end else begin
            rat_q <= rat_d;
            retirement_rat_q <= retirement_rat_d;
        end

        rob_mask_q <= rob_mask_d;
        rob_bits_q <= rob_bits_d;
        serviced_q <= serviced_d;
    end
end

endmodule