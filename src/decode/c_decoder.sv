

/*
	Note :  The decoder implements some aspects from the latest draft at 
	https://github.com/riscv/riscv-isa-manual . This is done because 
	some of the revisions are easier to implement compared to previously
	and removes a few ambiguities from the previous text. 
*/

import rvp1_pkg::*;	

module c_decoder (
		input c_dec_t instr_i,
		input logic entry_valid_i,
		input branchpredict_pe_t bp_pe_i,
		input exception_t ex_i,

		//From CSR
		input riscv_pkg::priv_lvl_t priv_lvl_i, // Current Privilege Level
		output iqueue_entry_t instruction_o
	);

	logic illegal_instr;
	// this instruction is an environment call. It is handled like an exception. 
	logic ecall;
	// This instruction is a software breakpoint
	logic ebreak;
	// This instruction needs floating point rounding mode verification
	logic check_fprm;
	// Amount to scale for ldsp lwsp etc. instrctions
	enum logic [1:0] {
		B,H,W,D
	} imm_scaling;

	riscv_pkg::cinstruction_t instr;
	assign instr =  instr_i.instr;

	enum logic [3:0] {
		NOIMM, CIWADD4SP, CIADD16SP, CLSW, CLSD,
		CIIMM, CJIMM, CBBR, CBALU, CILDSP, 
		CILWSP, CIWSDSP, CIWSWSP, CILUI,
		CILI
	} imm_select;

	enum logic {
		IMM, UIMM
	} imm_ex;
	

	// Type of Extension used in immediate
	always_comb begin : c_decoder
		imm_select = NOIMM;
		imm_ex = UIMM;
		instruction_o.is_control_flow_instr_o = 1'b0;
		illegal_instr = 1'b0;
		instruction_o.pc = instr_i.vaddr;
		instruction_o.fu = NONE;
		instruction_o.op = ADD;
		instruction_o.rs1 = '0;
		instruction_o.rs2 = '0;
		instruction_o.rd = '0;
		instruction_o.is_compressed = 1'b1;
		instruction_o.use_zimm = 1'b0;
		instruction_o.use_pc = 1'b0;
		instruction_o.bp = bp_pe_i;
		ecall = 1'b0;
		ebreak = 1'b0;
		check_fprm = 1'b0;

		if(~ex_i.valid & entry_valid_i) begin
			case (instr.cltype.op)
				riscv_pkg::OpcodeC0: begin
					imm_ex = UIMM;
					instruction_o.rs1 = {2'b0, instr.cltype.rs1};
					if (~instr[15]) begin
						instruction_o.fu = LOAD;
						instruction_o.rd = {2'b0, instr.cltype.rd};
						unique case (instr.cltype.funct3)
							riscv_pkg::OpcodeC0Addi4spn: begin
								instruction_o.rd = {2'b0, instr.ciwtype.rd};
								instruction_o.rs1 = 5'd2; // stack pointer
								if(instr.ciwtype.imm == '0)
									illegal_instr = 1'b1;
								imm_select = CIWADD4SP;
								imm_ex = UIMM;
							end
							riscv_pkg::OpcodeC0Fld: begin
								instruction_o.op = FLD;
								imm_select = CLSD;
							end
							riscv_pkg::OpcodeC0Lw: begin
								instruction_o.op = LW;
								imm_select = CLSW;
							end
							riscv_pkg::OpcodeC0Ld: begin
								instruction_o.op = LD;
								imm_select = CLSD;
							end
							default: illegal_instr = 1'b1;
						endcase // instr.cltype.funct3
					end

					if(instr[15]) begin
						instruction_o.fu = STORE;
						instruction_o.rs2 = {2'b0, instr.cstype.rs2};
						unique case (instr.cstype.funct3)
							riscv_pkg::OpcodeC0Fsd: begin
								instruction_o.op = FSD;
								imm_select = CLSD;
							end
							riscv_pkg::OpcodeC0Sw: begin
								instruction_o.op = SW;
								imm_select = CLSW;
							end
							riscv_pkg::OpcodeC0Sd: begin
								instruction_o.op = SD;
								imm_select = CLSD;
							end
							default: illegal_instr = 1'b1;
						endcase // instr.cstype.funct3
					end 
				end

				riscv_pkg::OpcodeC1: begin
					if (~instr[15]) begin
						// Refer CI Encoding
						instruction_o.rs1 = instr.citype.rd;
						instruction_o.rd = instr.citype.rd;
					end
					unique case (instr.citype.funct3)
						riscv_pkg::OpcodeC1Addi: begin
							// Not true to v2.2 of riscv-spec
							// based on latest draft
							if(instr.citype.rd == '0 || {instr.citype.imm,instr.citype.imm0} == '0) 
								instruction_o.fu = NONE;
							instruction_o.fu = ALU;
							instruction_o.op = ADD;
							imm_select = CIIMM;
							imm_ex = UIMM;
						end

						riscv_pkg::OpcodeC1Addiw: begin
							if(instr.citype.rd == '0)
								illegal_instr = 1'b1;
							instruction_o.fu = ALU;
							instruction_o.op = ADDW;
							imm_select = CIIMM;
							imm_ex = IMM;
						end

						riscv_pkg::OpcodeC1Li: begin
							if(instr.citype.rd == '0)
								instruction_o.fu = NONE;
							instruction_o.fu = ALU;
							instruction_o.op = ADD;
							imm_ex = IMM;
							imm_select = CILI;
						end

						riscv_pkg::OpcodeC1LuiAddi16sp: begin
							instruction_o.fu = ALU;
							imm_ex = IMM;
							if({instr.citype.imm,instr.citype.imm0} == '0)
									illegal_instr = 1'b1;
							if(instr.citype.rd == 5'b00010) begin //addi16sp
								instruction_o.op = ADD;
								imm_select = CIADD16SP;
							end else begin
								instruction_o.op = LUIC;
								imm_select = CILUI;
							end
						end

						riscv_pkg::OpcodeC1MiscAlu: begin
							instruction_o.fu = ALU;
							instruction_o.rs1 = {2'b0, instr.cbtype.rs1};
							instruction_o.rd = {2'b0, instr.cbtype.rs1};
							if(instr[11:10] != 2'b11) begin
								imm_select = CBALU;
								case (instr[11:10])
									2'b00: begin
										instruction_o.op = SRL;
										if({instr[15],instr[6:2]} == '0)
											instruction_o.fu = NONE;
									end
									2'b01: begin
										if({instr[15], instr[6:2]} == '0) begin
											instruction_o.fu = NONE;
										end else begin
											instruction_o.op = SRA;
										end
									end
									2'b10: begin
										instruction_o.op = ANDL;
									end
									default: begin
									end
								endcase // instr[11:10]
							end else begin
								instruction_o.rs2 = {2'b0, instr.catype.rs2};
								instruction_o.rs1 = {2'b0, instr.catype.rs1};
								instruction_o.rd = {2'b0, instr.catype.rs1};
								unique case ({instr.catype.funct6[12],instr.catype.funct2})
									3'b000: begin
										instruction_o.op = SUB;
									end
									3'b001: begin
										instruction_o.op = XORL;
									end
									3'b010: begin
										instruction_o.op = ORL;
									end
									3'b011: begin
										instruction_o.op = ANDL;
									end
									3'b100: begin
										instruction_o.op = SUBW;
									end
									3'b101: begin
										instruction_o.op = ADDW;
									end
									default: begin
										illegal_instr = 1'b1;
									end
								endcase // instr.catype.funct6
							end
						end
						riscv_pkg::OpcodeC1Beqz: begin
							instruction_o.fu = CTRL_FLOW;
							instruction_o.op = EQ;
							instruction_o.rs1 = {2'b0, instr.cbtype.rs1};
							instruction_o.rs2 = '0; //rs2 taken as zero register
							imm_select = CBBR;
							imm_ex = IMM;
							instruction_o.is_control_flow_instr_o = 1'b1;
						end
						riscv_pkg::OpcodeC1Bnez: begin
							instruction_o.fu = CTRL_FLOW;
							instruction_o.op = NE;
							instruction_o.rs1 = {2'b0, instr.cbtype.rs1};
							instruction_o.rs2 = '0; //rs2 taken as zero register
							imm_select = CBBR;
							imm_ex = IMM;
							instruction_o.is_control_flow_instr_o = 1'b1;
						end
						default: illegal_instr = 1'b1;
					endcase // instr.citype.funct3
				end

				riscv_pkg::OpcodeC2: begin
					unique case (instr.citype.funct3)
						riscv_pkg::OpcodeC2Slli: begin
							instruction_o.op = SLL;
							if(instr.citype.rd != 0)
								instruction_o.fu = ALU;
							else
								instruction_o.fu = NONE;
							if({instr.citype.imm,instr.citype.imm0} == '0)
								instruction_o.fu = NONE;
							imm_ex = UIMM;
							imm_select = CIIMM;
						end
						riscv_pkg::OpcodeC2Fldsp: begin
							instruction_o.fu = LOAD;
							instruction_o.op = FLD;
							instruction_o.rs1 = 5'd2;
							imm_ex = UIMM;
							imm_select = CILDSP;
							if(instr.citype.rd == '0)
								instruction_o.rd = '0;
						end
						riscv_pkg::OpcodeC2Lwsp: begin
							instruction_o.fu = LOAD;
							instruction_o.op = LW;
							imm_ex = UIMM;
							imm_select = CILWSP;
							if(instr.citype.rd == '0)
								instruction_o.rd = '0;
						end
						riscv_pkg::OpcodeC2Ldsp: begin
							instruction_o.fu = LOAD;
							instruction_o.op = LD;
							imm_ex = UIMM;
							imm_select = CILDSP;
							if(instr.citype.rd == '0)
								illegal_instr = 1'b1;
						end
						riscv_pkg::OpcodeC2JalrMvAdd: begin
							if(instr.crtype.rs2 == '0) begin
								instruction_o.fu = CTRL_FLOW;
								instruction_o.is_control_flow_instr_o = 1'b1;
								instruction_o.op = JALR;
								instruction_o.rd = '0;
								instruction_o.rs1 = instr.crtype.rd;
								if(instr[12]) begin
									instruction_o.rd = 5'd1;
								end
							end else begin
								if(instr.crtype.rd != '0) begin
									instruction_o.fu = ALU;
									instruction_o.op = ADD;
									instruction_o.rd = instr.crtype.rd;
									instruction_o.rs1 = instr.crtype.rs2;
									if (~instr[12])
										instruction_o.rs2 = '0;
									else
										instruction_o.rs2 = instr.crtype.rd;
								end else begin
									ebreak = 1'b1;
								end
							end
						end
						default: illegal_instr = 1'b1;
					endcase // instr.citype.funct3
				end

				default: illegal_instr = 1'b1;
			endcase // instr.crtype.op

		end
		// Illegal Instruction = 0x00000000
		if(instr == '0)
			illegal_instr = 1'b1;
	end

	//-------------------------------
    // Sign/Zero Extend Immediate
    //-------------------------------
    always_comb begin : imm_extend
    	if(imm_select == NOIMM)
    		instruction_o.use_imm = 1'b0;
    	else
    		instruction_o.use_imm = 1'b1;
    	unique case (imm_select)
    		CIWADD4SP: begin
    			instruction_o.result = {54'b0, instr[10:7], instr[12:11], instr[5], instr[2], 2'b0};
    		end
    		CLSD: begin
    			instruction_o.result = {56'b0,instr[6:5],instr[12:10],3'b0};
    		end
 			CLSW: begin
 				instruction_o.result = {57'b0,instr[5],instr[12:10],instr[6],2'b0};
 			end
    		default: begin

    		end
    	endcase // imm_select
    end


	//-------------------------------
    // Exception Handling
    //-------------------------------

    always_comb begin : exception_handling
    	instruction_o.ex      = ex_i;
        instruction_o.valid   = ex_i.valid;
        // look if we didn't already get an exception in any previous
        // stage - we should not overwrite it as we retain order regarding the exception
        if (~ex_i.valid) begin
            // if we didn't already get an exception save the instruction here as we may need it
            // in the commit stage if we got a access exception to one of the CSR registers
            instruction_o.ex.tval  = {48'b0,instr};
            // instructions which will throw an exception are marked as valid
            // e.g.: they can be committed anytime and do not need to wait for any functional unit
            // check here if we decoded an invalid instruction or if the compressed decoder already decoded
            // a invalid instruction
            if (illegal_instr) begin
                instruction_o.valid    = 1'b1;
                instruction_o.ex.valid = 1'b1;
                // we decoded an illegal exception here
                instruction_o.ex.cause = riscv_pkg::ILLEGAL_INSTR;
            // we got an ecall, set the correct cause depending on the current privilege level
            end else if (ecall) begin
                // this instruction has already executed
                instruction_o.valid    = 1'b1;
                // this exception is valid
                instruction_o.ex.valid = 1'b1;
                // depending on the privilege mode, set the appropriate cause
                case (priv_lvl_i)
                    riscv_pkg::PRIV_LVL_M: instruction_o.ex.cause = riscv_pkg::ENV_CALL_MMODE;
                    riscv_pkg::PRIV_LVL_S: instruction_o.ex.cause = riscv_pkg::ENV_CALL_SMODE;
                    riscv_pkg::PRIV_LVL_U: instruction_o.ex.cause = riscv_pkg::ENV_CALL_UMODE;
                    default:; // this should not happen
                endcase
            end else if (ebreak) begin
                // this instruction has already executed
                instruction_o.valid    = 1'b1;
                // this exception is valid
                instruction_o.ex.valid = 1'b1;
                // set breakpoint cause
                instruction_o.ex.cause = riscv_pkg::BREAKPOINT;
            end
        end
    end

endmodule // c_decoder