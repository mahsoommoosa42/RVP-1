import rvp1_pkg::*;

module id_stage (
	input  logic                                clk_i              ,
	input  logic                                rst_ni             ,
	input  logic                                flush_i            ,
	// from IF
	input  frontend_fetch_t                     fetch_entry_i      ,
	output logic                                decoded_instr_ack_o,
	// TO Dispatch
	output iqueue_entry_t [INSTR_PER_FETCH-1:0] dispatch_entry_o   ,
	output logic                                is_ctrl_flow_o     ,
	input  logic                                dispatch_instr_ack_i  ,
	// from CSR
	input  riscv_pkg::priv_lvl_t                priv_lvl_i         ,
	input  riscv_pkg::xs_t                      fs_i               ,
	input  logic          [                2:0] frm_i              ,
	input  logic                                tvm_i              ,
	input  logic                                tw_i               ,
	input  logic                                tsr_i
);

	localparam int unsigned G_INSTR_PER_FETCH = INSTR_PER_FETCH/2; //RV64G INSTR Per Fetch
	exception_t ex;

	iqueue_entry_t [INSTR_PER_FETCH/2-1:0] g_instr_o;
	iqueue_entry_t [INSTR_PER_FETCH-1:0] c_instr_o;

	assign ex.valid = fetch_entry_i.page_fault;
	assign ex.cause = riscv_pkg::INSTR_PAGE_FAULT;
	assign ex.tval = fetch_entry_i.vaddr[0]; 

	struct packed {
		logic valid;
		iqueue_entry_t[INSTR_PER_FETCH-1:0] decoded_instr;
	} dispatch_q, dispatch_d;


	for (genvar i = 0; i < INSTR_PER_FETCH; i++) begin
		c_decoder i_c_decoder (
			.instr_i                      ({fetch_entry_i.instr[i][15:0],fetch_entry_i.vaddr[i]}),
			.entry_valid_i                (fetch_entry_i.entry_valid[i]                         
			                               & fetch_entry_i.instr_is_compressed[i]               ), 
			.bp_pe_i                      (fetch_entry_i.bp_pe                                  ),
			.ex_i                         (ex													),
			.priv_lvl_i                   (priv_lvl_i                                           ),
			.instruction_o                (c_instr_o[i]                                         ), 
		);
	end

	for (genvar i = 0; i < G_INSTR_PER_FETCH; i++) begin
		g_decoder i_g_decoder (
			.instr_i          ({fetch_entry_i.instr_i[2 * i], fetch_entry_i.vaddr[2 * i]}),
			.entry_valid_i    (fetch_entry_i.entry_valid_i[2 * i] 
								& !fetch_entry_i.instr_is_compressed[2 * i]),
			.bp_pe_i          (fetch_entry_i.bp_pe),
			.ex_i             (ex),
			//From CSR
			.priv_lvl_i       (priv_lvl_i),
			// Current Privilege Level
			.fs_i             (fs_i),
			// Floating Point Extension Status
			.frm_i            (frm_i),
			// Floating Point Dynamic Rounding Mode
			.tvm_i            (tvm_i),
			// Trap Virtual Memory
			.tw_i             (tw_i),
			// Timeout Wait
			.tsr_i            (tsr_i),
			// Trap Sret
					// Instruction Queue Entry
			.instruction_o    (g_instr_o[i])
		);
	end

	always_comb begin
		dispatch_d = dispatch_q;
		decoded_instr_ack_o = 1'b0;

		// Clear the valid flag if dispatch has acked the instr
		if (dispatch_instr_ack_i)
			dispatch_d.valid = 1'b0;

		// Retreive new instruction if fetch is valid and current instruction is invalid or
		// dispatch has acknowledged instruction. 
		if ((!dispatch_q.valid || dispatch_instr_ack_i) && fetch_entry_i.valid) begin
			decoded_instr_ack_o = 1'b1;
			dispatch_d.valid = 1'b1;
			for (int i = 0; i < INSTR_PER_FETCH; i++) begin
				if (i % 2 == 0)
					dispatch_d.decoded_instr[i] = fetch_entry_i.instr_is_compressed[i] ? 
											c_instr_o[i] : g_instr_o[i/2];
				else
					dispatch_d.decoded_instr[i] = c_instr_o[i];
			end
		end

		if (flush_i)
			dispatch_d.valid = 1'b0;
	end

	assign dispatch_entry_o = dispatch_q.decoded_instr[INSTR_PER_FETCH-1:0];

	always_ff @(posedge clk_i or negedge rst_ni) begin
		if(~rst_ni) begin
			dispatch_q <= '0;
		end else begin
			dispatch_q <= dispatch_d;
		end
	end


endmodule
