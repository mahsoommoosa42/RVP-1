module fifo #(
	parameter int  unsigned DEPTH      = 8            ,
	parameter int  unsigned DATA_WIDTH = 32           ,
	parameter type          dtype      = logic  [DATA_WIDTH:0],
	parameter int  unsigned ADDR_DEPTH = $clog2(DEPTH)
) (
	input  logic                  clk_i  , // Clock input
	input  logic                  rst_ni , // Asynchronous Reset Active Low
	input  logic                  flush_i,
	input  dtype                  data_i ,
	output dtype                  data_o ,
	input  logic                  push_i ,
	input  logic                  pop_i  ,
	output logic                  full_o ,
	output logic                  empty_o,
	output logic [ADDR_DEPTH:0]   usage_o
);
	
	//FIFO Memory
	dtype [DEPTH-1:0] mem_q,mem_d;

	//Read and Write Pointers
	logic [ADDR_DEPTH-1:0] read_pointer_q,read_pointer_d;
	logic [ADDR_DEPTH-1:0] write_pointer_q,write_pointer_d;

	//Usage 
	logic [ADDR_DEPTH:0] usage_q,usage_d;

	//Clock Gating Control
	logic clock_gate;

	//Full and Empty Logic
	assign empty_o = (usage_q == 0);
	assign full_o = (usage_q == DEPTH[ADDR_DEPTH:0]);

	always_comb begin
		mem_d = mem_q;
		read_pointer_d = read_pointer_q;
		write_pointer_d = write_pointer_q;
		data_o = mem_q[read_pointer_q];
		clock_gate = 1'b1;

		if(push_i && ~full_o) begin
			mem_d[write_pointer_q] = data_i;
			clock_gate = 1'b0;
			if(write_pointer_q == DEPTH[ADDR_DEPTH-1:0] - 1) begin
				write_pointer_d = '0;
			end else begin
				write_pointer_d = write_pointer_q + 1;
			end
		end

		if(pop_i && ~empty_o) begin
			if(read_pointer_q == DEPTH[ADDR_DEPTH-1:0] - 1) begin
				read_pointer_d = '0;
			end else begin
				read_pointer_d = read_pointer_q + 1;
			end
		end
	end

	always_comb begin
		usage_d = usage_q;
		if(push_i && ~full_o) begin
			usage_d = usage_q + 1;
		end

		if(pop_i && ~empty_o) begin
			usage_d = usage_q - 1;
		end

		if(push_i && pop_i && ~full_o && ~empty_o) begin
			usage_d = usage_q;
		end
	end

	assign usage_o = usage_q;

	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_read_pointer_q
		if(~rst_ni) begin
			read_pointer_q <= '0;
			write_pointer_q <= '0;
			usage_q <= '0;
		end else begin
			if(flush_i) begin
				read_pointer_q <= '0;
				write_pointer_q <= '0;
				usage_q <= '0;
			end else begin
				read_pointer_q <= read_pointer_d;
				write_pointer_q <= write_pointer_d;
				usage_q <= usage_d;
			end
		end
	end

	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_mem_q
		if(~rst_ni) begin
			mem_q <= '0;
		end else begin
			if(flush_i) begin
				for (int i = 0; i < DEPTH; i++) begin
					mem_q[i] <= '0;
				end
			end else begin
				if(!clock_gate) begin
					mem_q <= mem_d;
				end
			end
		end
	end

endmodule