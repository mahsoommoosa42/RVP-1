// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 10-03-19
// Description : RVP-1
import rvp1_pkg::*;

module rvp1 (
	input clk_i,    // Clock
	input rst_ni, // Asynchronous Reset Active Low
	input [63:0] boot_addr_i // Boot Address
);

	// Instruction Fetch Front End

	icache_dreq_i_t dreq_if_cache;
	icache_dreq_o_t dreq_cache_if;

	frontend_fetch_t fetch_entry_o;

	frontend i_frontend (
		.clk_i             (clk_i               ),
		.rst_ni            (rst_ni              ),
		.flush_i           (1'b0                ),
		.flush_bp_i        (1'b0                ),
		.icache_dreq_o     (dreq_if_cache       ),
		.icache_dreq_i     (dreq_cache_if       ),
		.boot_addr_i       (64'hffffffffffffffff),
		.ex_valid_i        (1'b0                ),
		.trap_vector_base_i(64'd0               ),
		.set_pc_commit_i   (1'b0                ),
		.pc_commit_i       (64'd0               ),
		.eret_i            (1'b0                ),
		.epc_i             (64'd0               ),
		.fetch_ack_i       (1'b0                ),
		.resolved_branch_i ('0                  ),
		.fetch_entry_o     (fetch_entry_o       )
	);


	// ID Stage

	// Issue Stage

	// Exec Stage

	// Commit Stage

	// Cache Subsystem

	//-------------------------
	// CSR Regfile
	//-------------------------
	


	cache_subsystem i_cache_subsystem (
		.clk_i         (clk_i         ),
		.rst_ni        (rst_ni        ),
		.icache_dreq_i (dreq_if_cache ),
		.icache_dreq_o (dreq_cache_if ),
	);


	// Main Control Unit

endmodule