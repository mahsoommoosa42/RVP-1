// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 28-02-19
// Description : Defines

package rvp1_pkg;

	//---------------
	//Global Config
	//---------------

	localparam NR_IQ_ENTRIES = 32; // Number of Instruction Queue Entries
	localparam TRANS_ID_BITS = $clog2(NR_IQ_ENTRIES);

	localparam NR_FP_IQ_ENTRIES = 16; // Number of Floating Point Queue Entries
	localparam TRANS_FP_BITS = $clog2(NR_FP_IQ_ENTRIES);

	localparam NR_LD_STR_IQ_ENTRIES = 8; // Number of Load Store Queue Entries
	localparam TRANS_LD_STR_BITS = $clog2(NR_LD_STR_IQ_ENTRIES);

	localparam ASID_WIDTH = 1;
	localparam BTB_ENTRIES = 128;
	localparam BHT_ENTRIES = 256;
	localparam RAS_DEPTH = 4;
	localparam BITS_SATURATION_COUNTER = 2;
	localparam NR_COMMIT_PORTS = 2;

	// WFI Status
	localparam bit ENABLE_WFI = 1'b1;

	// Floating-point extensions configurations
	localparam bit RVF = 1'b1;	// F extension enabled
	localparam bit RVD = 1'b1;	// D extension enabled
	localparam bit RVA = 1'b1;	// A extension enabled

	localparam bit FP_PRESENT = RVF | RVD;

	localparam bit NSX = 1'b0; // Non-standard extension present

	// Length of widest floating point format
	localparam FLEN =	RVD	?	64 : // D ext.
						RVF ?	32 : // F ext.
						0;	// Unused in case of no FP

	localparam logic [63:0] ISA_CODE = (RVA <<  0)  // A - Atomic Instructions extension
                                     | (1   <<  2)  // C - Compressed extension
                                     | (RVD <<  3)  // D - Double precsision floating-point extension
                                     | (RVF <<  5)  // F - Single precsision floating-point extension
                                     | (1   <<  8)  // I - RV32I/64I/128I base ISA
                                     | (1   << 12)  // M - Integer Multiply/Divide extension
                                     | (0   << 13)  // N - User level interrupts supported
                                     | (1   << 18)  // S - Supervisor mode implemented
                                     | (1   << 20)  // U - User mode implemented
                                     | (NSX << 23)  // X - Non-standard extensions present
                                     | (1   << 63); // RV64

    
    localparam NR_ARF = 32;
    localparam NR_ARF_ADDR_BITS = $clog2(NR_ARF);

    localparam NR_FP_ARF = 32;
    localparam NR_FP_ARF_ADDR_BITS = $clog2(NR_FP_ARF);

    localparam NR_RRF = 64;
    localparam NR_RRF_ADDR_BITS = $clog2(NR_RRF);

    localparam NR_FP_RRF = 64;
    localparam NR_FP_RRF_ADDR_BITS = $clog2(NR_FP_ARF);


	//-----------------------------
	//----Fetch Stage--------------
	//-----------------------------

	//Fetch FIFO Size
	localparam int unsigned FETCH_FIFO_DEPTH = 8;
	// Bits Retrieved per Fetch
	localparam int unsigned FETCH_WIDTH = 64;
	// Maximum Number of Instructions per Fetch. Max = 2 Compressed Instructions
	localparam int unsigned INSTR_PER_FETCH = FETCH_WIDTH / 16;
	// Issue Width
	localparam int unsigned ISSUE_WIDTH = 2;
	// XLEN
	localparam int unsigned XLEN = 64;



	// --------------------------------------------
	// EX Stage
	// --------------------------------------------

	// Functional Operations
	typedef enum logic[3:0] {
		NONE,		//0
		LOAD,		//1
		STORE,		//2
		ALU,		//3
		CTRL_FLOW,	//4
		MULT,		//5
		CSR,		//6
		FPU,		//7
		FPU_VEC		//8
	} fu_t;

	typedef enum logic [6:0] { //basic ALU op
							ADD, SUB, ADDW, SUBW,
							// logic operations
							XORL, ORL, ANDL,
							// shifts
							SRA, SRL, SLL, SRLW, SLLW, SRAW,
							// Load Unsigned Immediate
							LUI, LUIC,
							// comparisons
							LTS, LTU, GES, GEU, EQ ,NE,
							// jumps
							JALR,
							// set lower than operations
							SLTS, SLTU,
							// CSR functions
							MRET, SRET, DRET, ECALL, WFI, FENCE, FENCE_I, 
							SFENCE_VMA, CSR_WRITE, CSR_READ, CSR_SET, CSR_CLEAR,
							// Load Store Unit
							LD, SD, LW, LWU, SW, LH, LHU, SH, LB, SB, LBU,
							// Atomic Memory Operations
	                        AMO_LRW, AMO_LRD, AMO_SCW, AMO_SCD,
	                        AMO_SWAPW, AMO_ADDW, AMO_ANDW, AMO_ORW, AMO_XORW, AMO_MAXW, AMO_MAXWU, AMO_MINW, AMO_MINWU,
	                        AMO_SWAPD, AMO_ADDD, AMO_ANDD, AMO_ORD, AMO_XORD, AMO_MAXD, AMO_MAXDU, AMO_MIND, AMO_MINDU,
							// Multiplications
							MUL, MULH, MULHU, MULHSU, MULW,
							// Divisions
							DIV, DIVU, DIVW, DIVUW, REM, REMU, REMW, REMUW,
							// Floating-Point Load and Store Instructions
                            FLD, FLW, FLH, FLB, FSD, FSW, FSH, FSB,
                            // Floating-Point Computational Instructions
                            FADD, FSUB, FMUL, FDIV, FMIN_MAX, FSQRT, FMADD, FMSUB, FNMSUB, FNMADD,
                            // Floating-Point Conversion and Move Instructions
                            FCVT_F2I, FCVT_I2F, FCVT_F2F, FSGNJ, FMV_F2X, FMV_X2F,
                            // Floating-Point Compare Instructions
                            FCMP,
                            // Floating-Point Classify Instruction
                            FCLASS,
                            // Vectorial Floating-Point Instructions that don't directly map onto the scalar ones
                            VFMIN, VFMAX, VFSGNJ, VFSGNJN, VFSGNJX, VFEQ, VFNE, VFLT, VFGE, VFLE, VFGT, VFCPKAB_S, VFCPKCD_S, VFCPKAB_D, VFCPKCD_D
							} fu_op;


	//-----------------------
	//Fetch Stage
	//-----------------------


	// Control Flow Instruction Options
	typedef enum logic [1:0] {BHT,BTB,RAS} cf_t;

	typedef struct packed {
		logic [63:0] pc            ;
		logic [63:0] target_address;
		logic        is_mispredict ;
		logic        is_taken      ;
		logic        valid         ;
		logic        clear         ;
		cf_t         cf_type       ;
	} branchpredict_t;

	typedef struct packed {
		logic                       valid       ;
		logic [               63:0] predict_addr;
		logic                       b_taken     ;
		logic [INSTR_PER_FETCH-1:0] which_branch;
		cf_t                        cf_type     ;
	} branchpredict_pe_t;

	typedef struct packed {
		logic [63:0] cause; //cause of exception
		logic [63:0] tval ; //additional information regarding exception
		logic        valid;
	} exception_t;

	typedef struct packed {
		logic        valid         ;
		logic [63:0] pc            ;
		logic [63:0] target_address;
		logic        clear         ;
	} btb_update_t;

	typedef struct packed {
		logic        valid         ;
		logic [63:0] target_address;
	} btb_predict_t;

	typedef struct packed {
		logic			valid;
		logic [63:0]	pc;
		logic 			mispredict;
		logic			taken;
	} bht_update_t;

	typedef struct packed {
		logic			valid;
		logic 			taken;
		logic			strongly_taken;
	} bht_predict_t;

	typedef struct packed {
		logic			valid;
		logic [1:0]		sat_cnt;
	} bht_entry_t;

	typedef struct packed {
		logic			valid;
		logic[63:0]		ra;
	} ras_t;

	typedef struct packed {
		logic        req    ;
		logic        kill_s1;
		logic        kill_s2;
		logic [63:0] vaddr  ;
	} icache_dreq_i_t;

	typedef struct packed {
		logic                   ready;
		logic                   valid;
		logic [FETCH_WIDTH-1:0] data ;
		logic [           63:0] vaddr;
		exception_t ex;
	} icache_dreq_o_t;

	typedef struct packed {
		logic                             valid              ;
		logic [INSTR_PER_FETCH-1:0][63:0] vaddr              ;
		logic [INSTR_PER_FETCH-1:0][31:0] instr              ;
		logic [INSTR_PER_FETCH-1:0]       entry_valid        ;
		logic [INSTR_PER_FETCH-1:0]       instr_is_compressed;
		branchpredict_pe_t                bp_pe              ;
		logic [INSTR_PER_FETCH-1:0]       bp_taken           ;
		logic	                          page_fault         ;
	} frontend_fetch_t;

	typedef struct packed {
		logic [31:0] instr;
		logic [63:0] vaddr;
	} g_dec_t;

	typedef struct packed {
		logic [15:0] instr;
		logic [63:0] vaddr;
	} c_dec_t;

	typedef struct packed {
		logic [             63:0] pc           ; //PC of instruction

		fu_t                         fu           ; //functional unit to use
		fu_op                        op           ; // operation to perform for each functional unit
		logic [NR_ARF_ADDR_BITS-1:0] rs1          ; // register source address 1
		logic [NR_ARF_ADDR_BITS-1:0] rs2          ; // register source address 2
		logic [NR_ARF_ADDR_BITS-1:0] rd           ; // register destination address
		logic [                63:0] result       ;
		logic                        valid        ; // is the result valid?
		logic                        use_imm      ; // immediate operand b
		logic                        use_zimm     ; // immediate operand a
		logic                        use_pc       ;
		exception_t                  ex           ;
		branchpredict_pe_t           bp           ;
		logic                        is_compressed;
		logic						 is_control_flow_instr_o;
	} iqueue_entry_t;

	//------------------------------
	// Issue Stage Entry
	//------------------------------
	typedef struct packed {
		logic [63:0] pc;
		logic valid;
		fu_t fu;
		fu_op op;
		logic [63:0] rs1_data;
		logic [63:0] rs2_data;
		logic [NR_RRF_ADDR_BITS-1:0] rob_no;
		logic [NR_ARF_ADDR_BITS-1:0] rd;
		logic [63:0] result;
		logic use_imm;
		logic use_zimm;
		logic use_pc;
		exception_t ex;
		branchpredict_pe_t bp;
		logic is_compressed;
		logic is_control_flow_instr_o;
	} issue_entry_t;

	//------------------------------
	// Reorder Buffer Entries
	//------------------------------	
	typedef struct packed {
		logic issued;
		logic finished;

		// bit to indicate whether 
		// this is the copy in 
		// retirement rat copy as well
		logic orig;

		// Newer version of AR exists
		logic old;
	} rob_entry_t;

	// Immediate Decoding functions

	function automatic logic [63:0] i_imm (logic [31:0] instr_i);
		return { { 53 { instr_i[11]}}, instr_i[30:20]};
	endfunction

	function automatic logic [63:0] s_imm (logic [31:0] instr_i);
		return { { 53 { instr_i[11]}}, instr_i[30:25], instr_i[11:7]};
	endfunction

	function automatic logic [63:0] b_imm (logic [31:0] instr_i);
		return { { 52 { instr_i[31]}}, instr_i[7], instr_i[30:25], instr_i[11:8], 1'b0};
	endfunction

	function automatic logic [63:0] u_imm (logic [31:0] instr_i);
		return { instr_i[31:12], 44'b0};
	endfunction

	function automatic logic [63:0] j_imm (logic [31:0] instr_i);
		return { { 44 { instr_i[31]}}, instr_i[19:12], instr_i[20], instr_i[30:21], 1'b0};
	endfunction

	function automatic logic [63:0] cj_imm (logic [31:0] instr_i);
		return { {52 {instr_i[12]}}, instr_i[12:2],1'b0};
	endfunction

	function automatic logic [63:0] cb_imm (logic [31:0] instr_i);
		return { {55 { instr_i[12]}}, instr_i[12], instr_i[6:5], instr_i[2], instr_i[11:10], instr_i[4:3], 1'b0};
	endfunction

endpackage