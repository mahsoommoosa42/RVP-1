module regfile #(parameter int unsigned NUM_REG = rvp1_pkg::INSTR_PER_FETCH) (
    input logic clk_i, 
    input logic rst_ni,

    // Read Ports
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0] rd_enable,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(NUM_REG)-1:0] read_reg_i,
    output logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][63:0] read_data_o,

    // Write Ports
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] wr_enable,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(NUM_REG)-1:0] write_reg_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][63:0] write_data_i
);

logic [NUM_REG-1:0][63:0] regs_q, regs_d;

for (genvar i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
    for (genvar j = 0; j < 2; j++) begin
        assign read_data_o[i][j] = rd_enable[i][j] ? regs_q[read_reg_i[i][j]] : '0;
    end
end

always_comb begin
    regs_d = regs_q;

    for (int unsigned i = 0; i < rvp1_pkg::INSTR_PER_FETCH; i++) begin
        if (wr_enable[i])
            regs_d[write_reg_i[i]] = write_data_i[i];
    end
end

always_ff @ (posedge clk_i or negedge rst_ni) begin
    if (~rst_ni) begin
        regs_q <= '0;
    end else begin
        regs_q <= regs_d;
    end
end


endmodule // regfile