module reorder_buff (
    input logic clk_i,
    input logic rst_ni,

    // Register Rename Ports
    input logic[rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] read_rename_reg_i,
    input logic[rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] write_rename_reg_i
);

rvp1_pkg::rob_entry_t [rvp1_pkg::NR_RRF-1:0] rob_d, rob_q;

always_ff @(posedge clk_i or negedge rst_ni) begin
    if (~rst_ni) begin
        rob_q <= '0;
    end else begin
        rob_q <= rob_d;
    end
end

endmodule // reorder_buff