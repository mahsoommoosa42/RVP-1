import rvp1_pkg::*;

module dispatch_regop(
    input logic clk_i,
    input logic rst_ni,
    input logic flush_i,

    input iqueue_entry_t[INSTR_PER_FETCH-1:0] decode_dispatch_entry_i,
    output logic dispatch_decode_ack_o,

    // Reg Status Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_ready_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0] ready_valid_i,
    output logic [rvp1_pkg::INSTR_PER_FETCH-1:0][1:0] reg_ready_o,

    // Issue Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_issued_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] issue_valid_i,

    // Exec Stage Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_finished_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] finish_valid_i,

    // Commit Stage Pins
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] reg_commit_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0] commit_valid_i,
    input logic [rvp1_pkg::INSTR_PER_FETCH-1:0][$clog2(rvp1_pkg::NR_RRF)-1:0] orig_reg_i
);

iqueue_entry_t[INSTR_PER_FETCH-1:0] collapsed_instr;
logic [INSTR_PER_FETCH-1:0] valid;

// Collapse entries
always_comb begin
    logic [$clog2(INSTR_PER_FETCH)-1:0] index = '0;
    collapsed_instr = '0;   
    for (int i = 0; i < INSTR_PER_FETCH; i++) begin
        if(decode_dispatch_entry_i[i].valid) begin
            collapsed_instr[index] = decode_dispatch_entry_i[i];
            index = index + 1;
        end
    end
end

logic [INSTR_PER_FETCH-1:0][1:0][NR_ARF_ADDR_BITS-1:0] read_reg_i;
logic [INSTR_PER_FETCH-1:0][1:0][NR_RRF_ADDR_BITS-1:0] read_rename_reg_o;


logic [INSTR_PER_FETCH-1:0][NR_ARF_ADDR_BITS-1:0] write_reg_i;
logic [INSTR_PER_FETCH-1:0][NR_RRF_ADDR_BITS-1:0] write_rename_reg_o;

for (genvar i = 0; i < INSTR_PER_FETCH; i++) begin
    assign valid[i] = collapsed_instr[i].valid;

    assign read_reg_i[i][0] = collapsed_instr[i].rs1;
    assign read_reg_i[i][1] = collapsed_instr[i].rs2;
    assign write_reg_i[i] = collapsed_instr[i].rd;

end


logic stall_for_instr_o;
logic rob_full_o;
assign dispatch_decode_ack_o = !(stall_for_instr_o || rob_full_o);


// Module for Register Renaming
register_rename u_register_rename(
	.clk_i              (clk_i              ),
    .rst_ni             (rst_ni             ),
    .flush_i            (flush_i            ),
    .valid              (valid              ),
    .read_reg_i         (read_reg_i         ),
    .read_rename_reg_o  (read_rename_reg_o  ),
    .write_reg_i        (write_reg_i        ),
    .write_rename_reg_o (write_rename_reg_o ),
    .rob_full_o         (rob_full_o         ),
    .reg_issued_i       (reg_issued_i       ),
    .issue_valid_i      (issue_valid_i      ),
    .reg_finished_i     (reg_finished_i     ),
    .finish_valid_i     (finish_valid_i     ),
    .reg_commit_i       (reg_commit_i       ),
    .commit_valid_i     (commit_valid_i     ),
    .orig_reg_i         (orig_reg_i         ),
    .reg_ready_i        (reg_ready_i        ),
    .ready_valid_i      (ready_valid_i      ),
    .reg_ready_o        (reg_ready_o        ),
    .stall_for_instr_o  (stall_for_instr_o  )
);

endmodule //dispatch