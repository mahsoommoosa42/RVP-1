// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 28-02-19
// Description : Branch Target Buffer

module btb #(parameter int unsigned NUM_ENTRIES = 512) (
	input	logic					clk_i,					//	Clock
	input	logic					rst_ni,					//	Asynchronous Reset Active Low
	input	logic					flush_i,				//  Flush BTB
	input	logic [63:0]			pc_i,					//	PC from IF Stage
	input	rvp1_pkg::btb_update_t 	btb_update_i,			//	Update BTB
	output	rvp1_pkg::btb_predict_t btb_predict_o			//	Prediction from BTB
);
	localparam OFFSET = $clog2(rvp1_pkg::FETCH_WIDTH/8);
	localparam PREDICTION_BITS = $clog2(NUM_ENTRIES) + OFFSET;

	logic [$clog2(NUM_ENTRIES) - 1:0] index,update_pc;

	rvp1_pkg::btb_predict_t [NUM_ENTRIES - 1 : 0] btb_d, btb_q;

	// Index of BTB
	assign index     = pc_i [PREDICTION_BITS - 1 : OFFSET];
	assign update_pc = btb_update_i.pc [PREDICTION_BITS - 1 : OFFSET];

	// Output Matching Prediction
	assign btb_predict_o = btb_q[index];

	always_comb begin : update_bp
		btb_d = btb_q;
		if (btb_update_i.valid) begin
			btb_d[update_pc].target_address = btb_update_i.target_address;
			btb_d[update_pc].valid = 1'b1;

			if(btb_update_i.clear) begin
				btb_d[update_pc].valid = 1'b0;
			end
		end
	end


	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_btb_q
		if(~rst_ni) begin
			for (int i = 0; i < NUM_ENTRIES; i++) begin
				btb_q[i] <= '0;
			end
		end else begin
			// Flush all Entries
			if (flush_i) begin
				for (int i = 0; i < NUM_ENTRIES; i++) begin
					btb_q[i].valid <= 1'b0;
				end
			end else begin
				btb_q <= btb_d;
			end
		end
	end


endmodule // btb