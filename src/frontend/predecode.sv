// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 28-02-19
// Description : Predecoder

module predecode (
	input	logic [31:0]		instr_i,
	output	logic				is_rvc_o,
	output	logic				rvi_return_o,
	output	logic				rvi_call_o,
	output	logic				rvi_branch_o,
	output	logic				rvi_jalr_o,
	output	logic				rvi_jump_o,
	output	logic [63:0]		rvi_imm_o,
	output	logic				rvc_branch_o,
	output	logic				rvc_jump_o,
	output	logic				rvc_jr_o,
	output	logic				rvc_return_o,
	output	logic				rvc_jalr_o,
	output	logic				rvc_call_o,
	output	logic [63:0]		rvc_imm_o			
	);

	// Check if instruction is compressed
	assign is_rvc_o = (instr_i[1:0] != 2'b11);

	// Function Calls and Returns. 
	// Check if rs1 is x1 or x5. 
	assign rvi_return_o = rvi_jalr_o & ({instr_i[19:18],instr_i[16:15]}==4'b0001) & ~instr_i[7];
	// Check if rd is either x1 or x5. and rs1 is neither x1 or x5. 
	assign rvi_call_o = (rvi_jump_o | rvi_jalr_o) & ({instr_i[11:10],instr_i[8:7]}==4'b0001);

	// Immediate Pre-decode RVI. Check if Opcode is JAL or BRANCH. JALR is obtained from BTB as target address cannot be obtained at this stage
	assign rvi_imm_o = instr_i[3] ? rvp1_pkg::u_imm(instr_i) : rvp1_pkg::b_imm(instr_i);	

	assign rvi_branch_o = (instr_i[6:0] == riscv_pkg::OpcodeBranch);
	assign rvi_jump_o = (instr_i[6:0] == riscv_pkg::OpcodeJal);
	assign rvi_jalr_o = (instr_i[6:0] == riscv_pkg::OpcodeJalr);

	// Immediate Pre-decode RVI. Check if Opcode is JAL or BRANCH. JALR is obtained from BTB as target address cannot be obtained at this stage
	assign rvc_imm_o = (instr_i[14]) ? rvp1_pkg::cb_imm(instr_i) : rvp1_pkg::cj_imm(instr_i);

	assign rvc_branch_o = (instr_i[1:0] == riscv_pkg::OpcodeC1) & 
					((instr_i[15:13] == riscv_pkg::OpcodeC1Beqz) | (instr_i[15:13] == riscv_pkg::OpcodeC1Bnez));
	assign rvc_jalr_o = (instr_i[1:0] == riscv_pkg::OpcodeC2) & 
						(instr_i[15:13] == riscv_pkg::OpcodeC2JalrMvAdd) &
						(instr_i[12] == 1'b1) & (instr_i[6:2]==5'b00000);
	assign rvc_jump_o = (instr_i[1:0] == riscv_pkg::OpcodeC1) & (instr_i[15:13] == riscv_pkg::OpcodeC1J);
	assign rvc_jr_o = (instr_i[1:0] == riscv_pkg::OpcodeC2) & 
						(instr_i[15:13] == riscv_pkg::OpcodeC2JalrMvAdd) &
						(instr_i[12] == 1'b0) & (instr_i[6:2]==5'b00000);

	// Compressed Instruction Function Calls						
	assign rvc_call_o = rvc_jalr_o;
	// Check if rs1 is x1 or x5. 
	assign rvc_return_o = rvc_jr_o & ({instr_i[11:10],instr_i[8:7]} == 4'b0001);
endmodule // predecode