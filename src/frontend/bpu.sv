	// Author : Moosa Mahsoom, BITS Pilani Goa Campus
	// Date : 28-02-19
	// Description : Branch Predict Unit

	import rvp1_pkg::*;
	module bpu (
		input  logic                             clk_i            ,
		input  logic                             rst_ni           ,
		input  logic                             flush_i          ,
		input  logic [               63:0]       bpc_i            ,
		input  logic [INSTR_PER_FETCH-1:0][31:0] instr            ,
		input  logic [INSTR_PER_FETCH-1:0][63:0] addr             ,
		input  logic [INSTR_PER_FETCH-1:0]       entry_valid      ,
		//from predecode
		input  logic [INSTR_PER_FETCH-1:0]       rvi_return       ,
		input  logic [INSTR_PER_FETCH-1:0]       rvi_call         ,
		input  logic [INSTR_PER_FETCH-1:0]       rvi_branch       ,
		input  logic [INSTR_PER_FETCH-1:0]       rvi_jalr         ,
		input  logic [INSTR_PER_FETCH-1:0]       rvi_jump         ,
		input  logic [INSTR_PER_FETCH-1:0][63:0] rvi_imm          ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_branch       ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_jump         ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_jr           ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_return       ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_jalr         ,
		input  logic [INSTR_PER_FETCH-1:0]       rvc_call         ,
		input  logic [INSTR_PER_FETCH-1:0][63:0] rvc_imm          ,
		input  branchpredict_t                   resolved_branch_i,
		output logic                             bp_valid         ,
		output logic [               63:0]       bp_vaddr         ,
		output branchpredict_pe_t                bp_pe            ,
		output logic                             is_mispredict
	);


		logic ras_pop,ras_push;
		logic [63:0] ras_update;
		rvp1_pkg::ras_t ras_predict;

		rvp1_pkg::bht_update_t bht_update_i;
		rvp1_pkg::bht_predict_t bht_predict_o;

		rvp1_pkg::btb_update_t btb_update_i;
		rvp1_pkg::btb_predict_t btb_predict_o;

		logic [INSTR_PER_FETCH-1:0] i_ras_pop;
		logic [INSTR_PER_FETCH-1:0] i_ras_push;
		logic [INSTR_PER_FETCH-1:0][63:0] i_ras_update;

		logic [INSTR_PER_FETCH-1:0] i_branch_taken;
		logic [INSTR_PER_FETCH-1:0][63:0] i_branch_address;

		cf_t [INSTR_PER_FETCH-1:0] i_cf_type;

		always_comb begin : b_predict1
			automatic logic [INSTR_PER_FETCH-1:0] take_rvi_cf; // CF Change from RVI insturction
			automatic logic [INSTR_PER_FETCH-1:0] take_rvc_cf; // CF Change from RVC instruction

			take_rvi_cf = '0;
			take_rvi_cf = '0;

			ras_pop = 1'b0;
			ras_push = 1'b0;
			ras_update = '0;

			bp_vaddr = '0;
			bp_valid = 1'b0;

			bp_pe.cf_type = RAS;

			i_ras_pop = '0;
			i_ras_push = '0;
			i_ras_update = '0;

			i_branch_taken = '0;
			i_branch_address = '0;

			i_cf_type = {RAS,RAS,RAS,RAS};

			for (int unsigned i = 0; i < INSTR_PER_FETCH; i++) begin
				if(entry_valid[i]) begin
					if(rvc_call[i] | rvi_call[i]) begin
						i_ras_push[i] = 1'b1;
						i_ras_update[i] = addr[i] + (rvc_call[i] ? 2 : 4);
					end

					if(rvc_return[i] | rvi_return[i]) begin
						if(ras_predict.valid) begin
							i_ras_pop[i] = 1'b1;
							i_branch_address[i] = ras_predict.ra;
							i_branch_taken[i] = 1'b1;
						end
					end

					if(rvi_branch[i] | rvc_branch[i]) begin
						i_cf_type[i] = BHT;
						if(bht_predict_o.valid) begin
							take_rvi_cf[i] = rvi_branch[i] & (bht_predict_o.strongly_taken | bht_predict_o.taken);
							take_rvc_cf[i] = rvc_branch[i] & (bht_predict_o.strongly_taken | bht_predict_o.taken);
						end
					end

					if(rvi_jump[i] | rvc_jump[i]) begin
						take_rvi_cf[i] = rvi_jump[i];
						take_rvc_cf[i] = rvc_jump[i];
					end

					if(rvi_jalr[i] | rvc_jalr[i] | rvc_jr[i]) begin
						i_cf_type[i] = BTB;
						if(btb_predict_o.valid) begin
							i_branch_taken[i] = 1'b1;
							i_branch_address[i] = btb_predict_o.target_address;
						end
					end 

					if(take_rvi_cf[i]) begin
						i_branch_taken[i] = 1'b1;
						i_branch_address[i] = addr[i] + rvi_imm[i];
					end

					if(take_rvc_cf[i]) begin
						i_branch_taken[i] = 1'b1;
						i_branch_address[i] = addr[i] + rvc_imm[i];
					end
				end
			end

			for (int i = INSTR_PER_FETCH-1; i >= 0; i--) begin
				bp_vaddr = i_branch_taken[i] ? i_branch_address[i] : '0;
				ras_push = i_branch_taken[i] ? i_ras_push[i] : 1'b0;
				ras_pop = i_branch_taken[i] ?  i_ras_pop[i] : 1'b0;
				ras_update = i_branch_taken[i] ? i_ras_update[i] : '0;
				bp_pe.cf_type = i_branch_taken[i] ? i_cf_type[i] : RAS;
			end

			bp_pe.which_branch = i_branch_taken;
			bp_valid = |i_branch_taken;
			bp_pe.valid = bp_valid;
			bp_pe.predict_addr = bp_vaddr;
			bp_pe.b_taken = bp_valid;
		end


		assign bht_update_i.valid      = resolved_branch_i.valid & (resolved_branch_i.cf_type == BHT);
		assign bht_update_i.pc         = resolved_branch_i.pc;
		assign bht_update_i.mispredict = resolved_branch_i.is_mispredict;
		assign bht_update_i.taken      = resolved_branch_i.is_taken;

		bht #(.NUM_ENTRIES(BHT_ENTRIES)) i_bht (
			.clk_i        (clk_i        ),
			.rst_ni       (rst_ni       ),
			.flush_i      (flush_i      ),
			.pc_i         (bpc_i        ),
			.bht_update_i (bht_update_i ),
			.bht_predict_o(bht_predict_o)
		);


		assign btb_update_i.valid          = resolved_branch_i.valid & (resolved_branch_i.cf_type == BTB);
		assign btb_update_i.pc             = resolved_branch_i.pc;
		assign btb_update_i.target_address = resolved_branch_i.target_address;
		assign btb_update_i.clear          = resolved_branch_i.clear;

		btb #(.NUM_ENTRIES(BTB_ENTRIES)) i_btb (
			.clk_i        (clk_i        ),
			.rst_ni       (rst_ni       ),
			.flush_i      (flush_i      ),
			.pc_i         (bpc_i        ),
			.btb_update_i (btb_update_i ),
			.btb_predict_o(btb_predict_o)
		);



		ras #(.DEPTH(RAS_DEPTH)) i_ras (
			.clk_i (clk_i ),
			.rst_ni(rst_ni),
			.push_i(ras_push),
			.pop_i (ras_pop),
			.data_i(ras_update),
			.data_o(ras_predict)
		);

		assign is_mispredict = resolved_branch_i.valid & resolved_branch_i.is_mispredict;

	endmodule // bpu