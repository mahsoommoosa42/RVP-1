// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 28-02-19
// Description : Instruction Fetch Frontend

import rvp1_pkg::*;

module frontend (
	input  logic           clk_i             , //	Clock In
	input  logic           rst_ni            , //	Reset In
	input  logic           flush_i           , //	Flush request for PCGEN
	input  logic           flush_bp_i        , // 	Flush Branch Prediction
	output icache_dreq_i_t icache_dreq_o     , // 	TO Cache
	input  icache_dreq_o_t icache_dreq_i     , // 	From Cache
	input  logic [63:0]    boot_addr_i       , //	Boot Address
	input  logic           ex_valid_i        , //	Exception is Valid (From Commit)
	input  logic [63:0]    trap_vector_base_i, //	base of trap vector
	input  logic           set_pc_commit_i   , //	Take the PC from commit stage
	input  logic [63:0]    pc_commit_i       , //	PC of instruction in commit stage
	input  logic           eret_i            , //	Return from Exception
	input  logic [63:0]    epc_i             , //	Exception PC to which we need to return
	input  logic           fetch_ack_i       , //	Acknowledge from ID Stage
	input  branchpredict_t resolved_branch_i ,  //	From Controller signalling the success of a branch instruction
	output frontend_fetch_t fetch_entry_o		//  To ID Stage
);

	logic [FETCH_WIDTH-1:0] idata_q;

	logic iex_q;

	logic npc_rst_load_q;
	logic [63:0] npc_d,npc_q;
	logic if_ready;
	logic ivalid_q;

	logic [INSTR_PER_FETCH-1:0] instr_is_compressed;

	logic bp_valid;
	logic [63:0] bp_vaddr;
	branchpredict_pe_t bp_pe;
	logic is_mispredict;	

	logic [63:0] ivaddr_q;


	// Predecode Variables
	logic [INSTR_PER_FETCH-1:0]                      is_rvc    ;
	logic [INSTR_PER_FETCH-1:0]                      rvi_return;
	logic [INSTR_PER_FETCH-1:0]                      rvi_call  ;
	logic [INSTR_PER_FETCH-1:0]                      rvi_branch;
	logic [INSTR_PER_FETCH-1:0]                      rvi_jalr  ;
	logic [INSTR_PER_FETCH-1:0]                      rvi_jump  ;
	logic [INSTR_PER_FETCH-1:0][63:0]                rvi_imm   ;
	logic [INSTR_PER_FETCH-1:0]                      rvc_branch;
	logic [INSTR_PER_FETCH-1:0]                      rvc_jump  ;
	logic [INSTR_PER_FETCH-1:0]                      rvc_jr    ;
	logic [INSTR_PER_FETCH-1:0]                      rvc_return;
	logic [INSTR_PER_FETCH-1:0]                      rvc_jalr  ;
	logic [INSTR_PER_FETCH-1:0]                      rvc_call  ;
	logic [INSTR_PER_FETCH-1:0][63:0]                rvc_imm   ;

	logic fifo_push,fifo_pop,fifo_empty,fifo_ready;
	logic s2_eff_kill,issue_req,s2_in_flight_d,s2_in_flight_q,fetch_entry_valid_o;
	logic [$clog2(FETCH_FIFO_DEPTH):0] fifo_credits_d,fifo_credits_q;

	logic [INSTR_PER_FETCH-1:0][31:0] instr;
	logic [INSTR_PER_FETCH-1:0][63:0] addr;
	logic [INSTR_PER_FETCH-1:0] entry_valid;
	logic instruction_valid;

	assign instruction_valid = ivalid_q;

	//Instruction Realigner
	instr_realigner i_instr_realigner (
		.clk_i              (clk_i                ),
		.rst_ni             (rst_ni               ),
		.flush_i            (icache_dreq_o.kill_s2),
		.instruction_valid  (instruction_valid    ),
		.idata_i            (idata_q              ),
		.addr_i             (ivaddr_q             ),
		.instr_o            (instr                ),
		.addr_o             (addr                 ),
		.entry_valid_o      (entry_valid          ),
		.instr_is_compressed_o(instr_is_compressed  )
	);



	bpu i_bpu (
		.clk_i            (clk_i            ),
		.rst_ni           (rst_ni           ),
		.flush_i          (flush_bp_i       ),
		.bpc_i            (ivaddr_q         ),
		.instr            (instr            ),
		.addr             (addr             ),
		.entry_valid      (entry_valid      ),
		.rvi_return       (rvi_return       ),
		.rvi_call         (rvi_call         ),
		.rvi_branch       (rvi_branch       ),
		.rvi_jalr         (rvi_jalr         ),
		.rvi_jump         (rvi_jump         ),
		.rvi_imm          (rvi_imm          ),
		.rvc_branch       (rvc_branch       ),
		.rvc_jump         (rvc_jump         ),
		.rvc_jr           (rvc_jr           ),
		.rvc_return       (rvc_return       ),
		.rvc_jalr         (rvc_jalr         ),
		.rvc_call         (rvc_call         ),
		.rvc_imm          (rvc_imm          ),
		.resolved_branch_i(resolved_branch_i),
		.bp_vaddr         (bp_vaddr         ),
		.bp_pe            (bp_pe            ),
		.is_mispredict    (is_mispredict    ),
		.bp_valid         (bp_valid         )
	);



	assign icache_dreq_o.kill_s1 = is_mispredict | flush_i;
	assign icache_dreq_o.kill_s2 = icache_dreq_o.kill_s1 | bp_valid;
	assign fifo_push = ivalid_q;

	/*
	Selection of Next PC :
	The Next PC can be from :
	0. Default Assignment
	1. Branch Predict Taken
	2. Control Flow Change Request (Misprediction)
	3. Return from Environment Call
	4. Exception/Interrupt
	5. Pipeline Flush because of CSR side effects
	*/

	always_comb begin : npc_select
		automatic logic [63:0] fetch_address;

		if(npc_rst_load_q) begin
			npc_d = boot_addr_i;
			fetch_address = boot_addr_i;
		end else begin
			fetch_address = npc_q;
			npc_d = npc_q;
		end

		// 0. Default Assignment
		if (if_ready) begin
			npc_d = {fetch_address[63:3], 3'b0} + 'h8;
		end

		// 1. Branch Prediction
		if (bp_valid) begin
			fetch_address = bp_vaddr;
			npc_d = bp_vaddr;
		end
		
		// 2. Mispredict
		if (is_mispredict) begin
			npc_d = resolved_branch_i.target_address;
		end

		// 3. Return from Environment Call
		if (eret_i) begin
			npc_d = epc_i;
		end

		// 4. Exception/Interrupt
		if (ex_valid_i) begin
			npc_d = trap_vector_base_i;
		end

		// 5. Pipeline Flush due to CSR Side Effects
		// Due to a flush request due to CSR
		if (set_pc_commit_i) begin
			npc_d = pc_commit_i + 64'h4;
		end

		icache_dreq_o.vaddr = fetch_address;
	end

	//------------------------------
	// Credit-based fetch FIFO flow ctrl
	//---------------------------------
	assign fifo_credits_d = (flush_i) ? FETCH_FIFO_DEPTH[3:0] :
        (fifo_credits_q + {3'b0,fifo_pop} + {3'b0,s2_eff_kill} - {3'b0,issue_req});

	// Check whether there is a request in flight that is being killed now
	// If this is the case, we need to increment the credits by 1
	assign s2_eff_kill = s2_in_flight_q & icache_dreq_o.kill_s2;
	assign s2_in_flight_d = (flush_i) ? 1'b0 :
		(issue_req) ? 1'b1 : 
		(icache_dreq_i.valid) ? 1'b0 : 
		s2_in_flight_q;

	// Only enable counter if current request is not being killed
	assign issue_req = if_ready & (~icache_dreq_o.kill_s1);
	assign fifo_pop = fetch_ack_i & fetch_entry_valid_o;
	assign fifo_ready = (|fifo_credits_q);
	assign if_ready = icache_dreq_i.ready & fifo_ready;
	assign icache_dreq_o.req = fifo_ready;
	assign fetch_entry_valid_o = ~fifo_empty;

	for (genvar i = 0; i < INSTR_PER_FETCH; i++) begin
		predecode i_predecode (
			.instr_i     (instr[i]     ),
			.is_rvc_o    (is_rvc[i]    ),
			.rvi_return_o(rvi_return[i]),
			.rvi_call_o  (rvi_call[i]  ),
			.rvi_branch_o(rvi_branch[i]),
			.rvi_jalr_o  (rvi_jalr[i]  ),
			.rvi_jump_o  (rvi_jump[i]  ),
			.rvi_imm_o   (rvi_imm[i]   ),
			.rvc_branch_o(rvc_branch[i]),
			.rvc_jump_o  (rvc_jump[i]  ),
			.rvc_jr_o    (rvc_jr[i]    ),
			.rvc_return_o(rvc_return[i]),
			.rvc_jalr_o  (rvc_jalr[i]  ),
			.rvc_call_o  (rvc_call[i]  ),
			.rvc_imm_o   (rvc_imm[i]   )
		);
	end

	always_ff @(posedge clk_i or negedge rst_ni) begin
		if(~rst_ni) begin
			npc_q <= 0;	
			npc_rst_load_q <= 1'b1;
			ivalid_q <= 1'b0;
			ivaddr_q <= '0;
			idata_q <= '0;
			iex_q <= '0;
			fifo_credits_q <= FETCH_FIFO_DEPTH[3:0];
			s2_in_flight_q <= 1'b0;
		end else begin
			npc_q <= npc_d;
			npc_rst_load_q <= 1'b0;
			ivalid_q <= icache_dreq_i.valid;
			ivaddr_q <= icache_dreq_i.vaddr;
			idata_q <= icache_dreq_i.data;
			iex_q <= icache_dreq_i.ex.valid;
			fifo_credits_q <= fifo_credits_d;
			s2_in_flight_q <= s2_in_flight_d;
		end
	end



	// Replace with a proper instruction queue system
	fifo #(
		.dtype(frontend_fetch_t),
		.DEPTH(FETCH_FIFO_DEPTH)
	) i_fifo (
		.clk_i  (clk_i                           ),
		.rst_ni (rst_ni                          ),
		.flush_i(flush_i                         ),
		.data_i ({fetch_entry_valid_o,addr,instr,entry_valid,instr_is_compressed,bp_pe,iex_q}),
		.data_o (fetch_entry_o                   ),
		.push_i (fifo_push                       ),
		.pop_i  (fifo_pop                        ),
		.full_o (                                ),
		.empty_o(fifo_empty                      ),
		.usage_o(                                )
	);



endmodule
// Notes
// On FPGA, the stage has a delay of ~11 ns. Leading to a clock speed of around 90 MHz.
// Therefore, splitting into two stages can speed up the clock speed.
// Although, this wouldn't be an issue on an ASIC. It can still bring increases in 
// Clock Speed 