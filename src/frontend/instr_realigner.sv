import rvp1_pkg::*;

module instr_realigner(
		input logic clk_i,
		input logic rst_ni,
		input logic flush_i,
		input logic instruction_valid,
		input logic [63:0] idata_i,
		input logic [63:0] addr_i,
		output logic [INSTR_PER_FETCH-1:0][31:0] instr_o,
		output logic [INSTR_PER_FETCH-1:0][63:0] addr_o,
		output logic [INSTR_PER_FETCH-1:0] entry_valid_o,
		output logic [INSTR_PER_FETCH-1:0] instr_is_compressed_o 
	);

	logic unaligned_q,unaligned_d;
	logic [63:0] unaligned_address_q,unaligned_address_d;
	logic [15:0] unaligned_instr_q,unaligned_instr_d;
	logic [INSTR_PER_FETCH-1:0] entry_valid;
	logic [INSTR_PER_FETCH-1:0] instr_is_compressed;

	logic [INSTR_PER_FETCH-1:0][31:0] instr;
	logic [INSTR_PER_FETCH-1:0][63:0] addr;

	for (genvar i = 0; i < INSTR_PER_FETCH; i++) begin
		assign instr_is_compressed[i] = (idata_i[i * 16 +: 2] != 2'b11);
	end

	assign instr_is_compressed_o = instr_is_compressed & entry_valid_o;

	always_comb begin
		unaligned_d = unaligned_q;
		unaligned_address_d = unaligned_address_q;
		unaligned_instr_d = unaligned_instr_q;

		entry_valid=4'b0101;

		instr[0] = idata_i[31:0];
		addr[0] = addr_i;

		instr[1] = '0;
		addr[1] = addr_i + 64'b010;

		instr[2] = idata_i[63:32];
		addr[2] = addr_i + 64'b100;

		instr[3] = '0;
		addr[3] = addr_i + 64'b110;

		if(instruction_valid) begin
			if(unaligned_q || instr_is_compressed[0]) begin

				if(instr_is_compressed[0]) begin
					instr[0] = {16'b0,idata_i[15:0]};
					entry_valid[0] = 1'b1;
				end

				if(unaligned_q) begin
					instr[0] = {idata_i[15:0],unaligned_instr_q};
					addr[0] = unaligned_address_q;
				end

				if(instr_is_compressed[1]) begin
					instr[1] = {16'b0,idata_i[31:16]};
					entry_valid[1] = 1'b1;
					if(instr_is_compressed[2]) begin
						instr[2] = {16'b0,idata_i[47:32]};
						entry_valid[2] = 1'b1;
						if(instr_is_compressed[3]) begin
							unaligned_d = 1'b0;
							instr[3] = {16'b0,idata_i[63:48]};
							entry_valid[3] = 1'b1;
						end else begin
							unaligned_d = 1'b1;
							unaligned_instr_d = idata_i[63:48];
							unaligned_address_d = addr[3];
						end
					end
				end else begin
					// Since an uncompressed instruction would consume
					// indexes 1,2 although the instruction is at index 1
					// We are storing the result at index 2 while invalidating
					// the entry in 1. 
					instr[2] = idata_i[47:16];
					addr[2] = addr_i + 64'b001;
					if(instr_is_compressed[3]) begin
						unaligned_d = 1'b0;
						instr[3] = {16'b0,idata_i[63:48]};
						entry_valid[3] = 1'b1;
					end else begin
						unaligned_d = 1'b1;
						unaligned_instr_d = idata_i[63:48];
						unaligned_address_d = addr[3];
					end
				end
			end else begin
				if(instr_is_compressed[2]) begin
					instr[2]={16'b0,idata_i[47:32]};
					if(instr_is_compressed[3]) begin
						unaligned_d = 1'b0;
						instr[3] = {16'b0,idata_i[63:48]};
						entry_valid[3]=1'b1;
					end else begin
						unaligned_d = 1'b1;
						unaligned_instr_d = idata_i[63:48];
						unaligned_address_d = addr[3];		
					end
				end
			end

			if(flush_i) begin
				unaligned_d = 1'b0;
			end
		end

		if(!instruction_valid) begin
			entry_valid=4'b0000;
		end
	end

	always_comb begin : mask_instr
		entry_valid_o = entry_valid & ((2 ** (4 - addr_i[2:1]))-1);
	end

	assign instr_o = instr;
	assign addr_o = addr;

	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_realign
		if(~rst_ni) begin
			unaligned_q <= 0;
			unaligned_instr_q <= '0;
			unaligned_address_q <= '0;
		end else begin
			unaligned_q <= unaligned_d;
			unaligned_instr_q <= unaligned_instr_d;
			unaligned_address_q <= unaligned_address_d;
		end
	end

endmodule // instr_realigner

