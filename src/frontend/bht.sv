// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 28-02-19
// Description : Branch History Table


module bht #(parameter int unsigned NUM_ENTRIES = 512) (
	input  logic                   clk_i        , // Clock Input
	input  logic                   rst_ni       , // Asynchronous Reset
	input  logic                   flush_i      , // Flush Signal
	input  logic [63:0]            pc_i         , // PC from IF Stage
	input  rvp1_pkg::bht_update_t  bht_update_i , // Update BHT
	output rvp1_pkg::bht_predict_t bht_predict_o  // Predict Output from BHT
);

	rvp1_pkg::bht_entry_t  [NUM_ENTRIES - 1 : 0] bht_q, bht_d;

	localparam OFFSET = 1;
	localparam PREDICTION_BITS = $clog2(NUM_ENTRIES) + OFFSET;

	logic [1:0] sat_cnt;

	logic [$clog2(NUM_ENTRIES) - 1:0] index,update_pc;

	// Index and Update PC
	assign index     = pc_i[PREDICTION_BITS - 1 : OFFSET];
	assign update_pc = bht_update_i.pc[PREDICTION_BITS - 1 : OFFSET];


	// Output Assignments
	assign bht_predict_o.valid          = bht_q[index].valid;
	assign bht_predict_o.taken          = (bht_q[index].sat_cnt == 2'b10);
	assign bht_predict_o.strongly_taken = (bht_q[index].sat_cnt == 2'b11);

	always_comb begin : bht_update
		bht_d = bht_q;
		sat_cnt = bht_q[update_pc].sat_cnt;

		if (bht_update_i.valid) begin

			bht_d[update_pc].valid = 1'b1;

			if (bht_update_i.taken) begin
				if (sat_cnt != 2'b11) begin
					bht_d[update_pc].sat_cnt = sat_cnt + 1;
				end
			end else begin
				if (sat_cnt != 2'b00) begin
					bht_d[update_pc].sat_cnt = sat_cnt - 1;
				end
			end
		end
	end

	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_
		if(~rst_ni) begin
			for (int i = 0; i < NUM_ENTRIES; i++) begin
				bht_q[i].valid <= 1'b0;
				bht_q[i].sat_cnt <= 2'b10;
			end
		end else begin
			if (flush_i) begin
				for (int i = 0; i < NUM_ENTRIES; i++) begin
					bht_q[i].valid <= 1'b0;
				end
			end else begin
				bht_q <= bht_d;
			end

		end
	end

endmodule // bht