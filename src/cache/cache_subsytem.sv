// Author : Moosa Mahsoom, BITS Pilani Goa Campus
// Date : 25-03-19
// Description : Cache Subsystem

// Only Instruction Cache implemented for the time being

import rvp1_pkg::*;

module cache_subsystem (
	input  logic           clk_i        ,
	input  logic           rst_ni       ,
	input  icache_dreq_i_t icache_dreq_i,
	output icache_dreq_o_t icache_dreq_o
);

	//TODO (Moosa) : Passthrough from memory for the time being

	always_ff @(posedge clk_i or negedge rst_ni) begin : proc_
		if(~rst_ni) begin
			icache_dreq_o.ready <= 1'b0;
			icache_dreq_o.valid <= 1'b0;
			icache_dreq_o.data <= '0;
			icache_dreq_o.vaddr <= '0;
			icache_dreq_o.ex <= '0;
		end else begin
			icache_dreq_o.ready <= icache_dreq_i.req;
			icache_dreq_o.valid <= ~icache_dreq_i.req;
			icache_dreq_o.data <= {icache_dreq_i.kill_s1,icache_dreq_i.kill_s2,62'b0};
			icache_dreq_o.vaddr <= icache_dreq_i.vaddr;
			icache_dreq_o.ex <= '0;
		end
	end


endmodule // cache_subsytem