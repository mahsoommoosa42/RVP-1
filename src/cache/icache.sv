


import rvp1_pkg::*

module icache(
		input clk_i,
		input rst_ni,

		input flush_i,

		input icache_dreq_i_t dreq_i,
		output icache_dreq_o_t dreq_o
	);

endmodule // icache