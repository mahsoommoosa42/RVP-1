
SRC_DIR = /mnt/c/Users/mahso/Desktop/RVP-1/src
TESTBENCH_DIR = $(SRC_DIR)/testbench

#--------------------------------------
# Verilator opts
#--------------------------------------

VERILATOR = verilator
#--------------------------------------
# Cross compiler opts
#--------------------------------------

RISCV_PREFIX = riscv64-unknown-elf
GCC_PREFIX = $(RISCV_PREFIX)-gcc
# Preprocessor (Removes Comments and links include files to
CPP_PREFIX = $(RISCV_PREFIX)-cpp
# Assembler
AS_PREFIX = $(RISCV_PREFIX)-as
ARCH = "rv64imc"
# Linker
LD_PREFIX = $(RISCV_PREFIX)-ld
M_OPT = "elf64lriscv" # elf64-little riscv
# Objdump
DUMP_PREFIX = $(RISCV_PREFIX)-objdump
#Objcopy (Generates hex file for testbench)
COPY_PREFIX = $(RISCV_PREFIX)-objcopy

defines := include/riscv_pkg.sv	\
			include/rvp1_pkg.sv

defines := $(addprefix $(SRC_DIR), $(defines))